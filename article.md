---
title: "Mapa výsledků podle počtu volitelů: Jak vyhrál Donald Trump americké volby?"
perex: "Státy, které v předchozích volbách připadly demokratovi Baracku Obamovi, letos získal Donald Trump. Je mezi nimi průmyslový blok amerického Středozápadu i Florida, kde tradičně platí, že vítěz vyhrává i celé volby. Splnilo se to i tentokrát."
description: "Jako doplněk ke klasickým volebním mapám, které zobrazují geografické rozložení Spojených států, a ne jejich význam při volbě prezidenta, připravil Český rozhlas takzvaný kartogram. Velikost států je pozměněná tak, aby odpovídala počtu volitelů."
authors: ["Jan Cibulka", "Petr Kočí", "Michal Zlatkovský"]
published: "9. listopadu 2016"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "vysledky-voleb-usa"
socialimg: https://interaktivni.rozhlas.cz/vysledky-voleb-usa/media/kartogram.png
libraries: [topojson, jquery, "https://d3js.org/d3.v4.min.js"]
recommended:
  - link: https://interaktivni.rozhlas.cz/usvolby-explainer/
    title: 7 historických volebních momentů, které dovedly Ameriku k Trumpovi
    perex: Jak a proč se vyvíjela americká volební mapa? Kdy si demokraté a republikáni vyměnili pozice a co vlastně jejich soupeření předcházelo?
    image: https://interaktivni.rozhlas.cz/data/usvolby-explainer/www/media/socialimg.jpg
  - link: https://interaktivni.rozhlas.cz/usa-twitter/
    title: Tři reportéři na cestě předvolební Amerikou. Sledujte je krok za krokem 
    perex: Jde vlastně o rychlé zápisky z cest. Anebo chcete-li: o cestopis věku sociálních médií.
    image: https://interaktivni.rozhlas.cz/data/usa-tweety/www/media/socimg.jpg
---

Po dlouhých měsících vypjaté kampaně je jasno: prezidentem Spojených států se na další čtyři roky stal Donald Trump. Na počet hlasů sice [těsně zvítězila](http://www.nytimes.com/elections/results/president) Hillary Clintonová, ale vzhledem k americkému [nepřímému volebnímu systému](http://www.nytimes.com/elections/results/president) se výsledek volby s lidovým hlasováním rozchází - teprve popáté v historii. Kde se Trumpovi podařilo získat potřebných 270 volitelů a ke kterým kandidátům se přiklonili Američané v jednotlivých státech? Český rozhlas výsledky zpracoval ve speciální mapě.

<div data-bso=1></div>

Jak uvádějí [The New York Times](http://www.nytimes.com/interactive/2016/11/08/us/elections/how-trump-pushed-the-election-map-to-the-right.html), boj Trumpa s Clintonovou určovala hlavně propast mezi městy a venkovem. Trump pro republikány získal velkou část venkovských hlasů, zejména od bělošských obyvatel Středozápadu bez vysokoškolského vzdělání. Těm americký tisk přezdívá Trumpovi demokrati: jde o tradiční voliče demokratů, kteří ale tentokrát uvěřili Trumpovu slibu, že pro ně zajistí více pracovních míst a "vrátí Americe její velikost". Trump u bílých nevysokoškoláků bodoval nejen na Středozápadě, ale v celých Spojených státech.

Hillary Clintonová bodovala především ve velkých městech, odvrátila se však od ní maloměsta, zejména v průmyslových regionech. Demokraté také ztratili část podpory nízkopříjmových voličů, kteří od své tradiční strany odklonili k republikánům. Podporu, kterou hledala u etnických minorit - černochů, Hispánců a Asiatů - získala menší než Barack Obama v předchozích volbách, zatímco republikáni si svou bělošskou základnu víceméně udrželi. Clintonovou oproti očekávání některých médií nepodpořily dostatečně ani ženy, které volily přibližně stejně jako minule a předminule.

<i>Jako doplněk ke klasickým volebním mapám, které zobrazují správně především rozlohu a hůře už politickou váhu jednotlivých států, si můžete prohlédnout takzvaný kartogram. Velikost amerických států je v něm upravená tak, aby odpovídala počtu volitelů, kteří svým hlasem formálně rozhodují o novém prezidentovi. Poskytuje tak přesnější informaci o skutečném významu jednotlivých dílčích vítězství.</i> 

<aside class="big">
  <div id="mapa"></div>
</aside>

O celkovém výsledku rozhodly vedle klíčových "bitevních států" Floridy a Ohia právě hlasy ze Středozápadu, ze států Iowa, Wisconsin nebo Michigan. Ve všech zmíněných státech zvítězil v minulých dvou volbách Barack Obama, Clintonová si je však nedokázala udržet. Demokratická kandidátka oproti očekávání zabodovala na východním pobřeží v obvykle republikánské Virginii. Pomohl jí k tomu zřejmě i fakt, že z tohoto státu pochází její kandidát na viceprezidena Tim Kain. To jí však k vítězství nestačilo.